/******************************************************************************
* FILE: ser_array.c
* DESCRIPTION: 
*   Serial Example - Array Assignment - C Version
*   In this simple example, an array is initialized and values assigned.
* AUTHOR: Blaise Barney
* LAST REVISED:  04/02/05
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#define	ARRAYSIZE	16000000

main() {
int     i; 			/* loop variable */
float	data[ARRAYSIZE]; 	/* the intial array */

/************************* initializations *********************************/

  printf("\n*********** Starting Serial Example ************\n");
  fflush(stdout);

  /* Initialize the array */
  for(i=0; i<ARRAYSIZE; i++) 
    data[i] =  i * 1.0;

  /* Do a simple value assignment to each of the array elements */
  for(i=1; i < ARRAYSIZE; i++)
     data[i] = data[i] + i * 1.0;

    printf("Sample results\n");
    printf("   data[1]=%e\n",  data[1]);
    printf("   data[100]=%e\n",  data[100]);
    printf("   data[1000]=%e\n\n",  data[1000]);
    fflush(stdout);

  printf("All Done! \n");
}
