/*
  For each position in a string, the program displays the length of the largest 
  matching substring elsewhere in the string. 
  The program also displays the location of a largest match for each position.  
  Consider the string "babba" as an example. Starting at position 0, "ba" is 
  the largest substring with a match elsewhere in the string (position 3). 
*/

#include <omp.h>
#include <iostream>
#include <string>

static const size_t N = 21;

static void serialSubStringFinder(const std::string &str, size_t *max_array, size_t *pos_array);
static void parallelSubStringFinder(const std::string &str, size_t *max_array, size_t *pos_array);

int main()
{
  // initialization of the strings
  // str[0] = "a", str[1] = "b", str[2] = "ab", str[3] = "bab"
  // str[4] = "abbab", etc.
  std::string str[N] = { std::string("a"), std::string("b") };
  for (size_t i = 2; i < N; ++i) 
    str[i] = str[i-1] + str[i-2];
  
  std::string &stringToScan = str[N-1];
  size_t strSize = stringToScan.size();
  //  std::cout << "String to scan:" << stringToScan << std::endl;
  size_t *parallelMaxStr = new size_t[strSize];
  size_t *serialMaxStr = new size_t[strSize];
  size_t *parallelPosStr = new size_t[strSize];
  size_t *serialPosStr = new size_t[strSize];
  std::cout << " Done building string." << std::endl;

  double serial_t0 = omp_get_wtime();
  serialSubStringFinder(stringToScan, serialMaxStr, serialPosStr);
  double serial_t1 = omp_get_wtime();
  std::cout << " Done with serial version." << std::endl;
  
  double parallel_t0 = omp_get_wtime();
  parallelSubStringFinder(stringToScan, parallelMaxStr, parallelPosStr);
  double parallel_t1 = omp_get_wtime();
  std::cout << " Done with parallel version." << std::endl;

 for (size_t i = 0; i < strSize; ++i) {
   if (parallelMaxStr[i] != serialMaxStr[i] || parallelPosStr[i] != serialPosStr[i]) {
     std::cout << "ERROR: Serial and Parallel Results are Different!" << std::endl;
   }
 }
 std::cout << " Done validating results." << std::endl;

 std::cout << "Serial version ran in " << (serial_t1 - serial_t0) << " seconds" << std::endl
           << "Parallel version ran in " <<  (parallel_t1 - parallel_t0) << " seconds" << std::endl
           << "Resulting in a speedup of " << (serial_t1 - serial_t0) / (parallel_t1 - parallel_t0) << std::endl;
  
  return 0;
}

void serialSubStringFinder(const std::string &str, size_t *max_array, size_t *pos_array) 
{
  for ( size_t i = 0; i < str.size(); ++i ) {
    size_t max_size = 0, max_pos = 0; 
    for (size_t j = 0; j < str.size(); ++j) {
      if (j != i) {
	size_t limit = str.size() - ( i > j ? i : j );
	for (size_t k = 0; k < limit; ++k) {
	  if (str[i + k] != str[j + k]) 
	    break;
	  if (k > max_size) {
	    max_size = k;
	    max_pos = j;
	  }
	}
      }
    }
    max_array[i] = max_size;
    pos_array[i] = max_pos;
  }
}

void parallelSubStringFinder(const std::string &str, size_t *max_array, size_t *pos_array) 
{
  for ( size_t i = 0; i < str.size(); ++i ) {
    size_t max_size = 0, max_pos = 0; 
    for (size_t j = 0; j < str.size(); ++j) {
      if (j != i) {
	size_t limit = str.size() - ( i > j ? i : j );
	for (size_t k = 0; k < limit; ++k) {
	  if (str[i + k] != str[j + k]) 
	    break;
	  if (k > max_size) {
	    max_size = k;
	    max_pos = j;
	  }
	}
      }
    }
    max_array[i] = max_size;
    pos_array[i] = max_pos;
  }
}

