/*
  This program implements a simple molecular dynamics simulation, using the 
  velocity Verlet time integration scheme. 
  The particles interact with a central pair potential.
*/

#include <omp.h>
#include <iostream>
#include <string>
#include <math.h>
#include <cstdlib>
#include <stdio.h>

#ifndef M_PI_2
#define M_PI_2   1.57079632679489661923  /* pi/2 */
#endif

typedef double vnd_t[3];

static double v(double x);
static double dv(double x);
static void clearVector(int size, vnd_t *data);
static void initialize(int np, int nd, const vnd_t &box, vnd_t *pos, vnd_t *vel, 
		       vnd_t *acc); 
static double dist(int nd, const vnd_t &r1, const vnd_t &r2, vnd_t &dr); 
static double dot_prod(int n, const vnd_t &x, const vnd_t &y);
 
void serialCompute(int np, int nd, vnd_t *pos, vnd_t *vel, double mass, vnd_t *f, 
		    double *pot_p, double *kin_p);
void serialUpdate(int np, int nd, vnd_t *pos, vnd_t *vel, vnd_t *f, vnd_t *a, 
		   double mass, double dt);
void serialSimulation(int nbParticules, int nbSteps, double mass, double dt, 
			     vnd_t *position, vnd_t *velocity, 
			     vnd_t *accel, vnd_t *force);

void parallelCompute(int np, int nd, vnd_t *pos, vnd_t *vel, double mass, vnd_t *f, 
		    double *pot_p, double *kin_p);
void parallelUpdate(int np, int nd, vnd_t *pos, vnd_t *vel, vnd_t *f, vnd_t *a, 
		   double mass, double dt);
void parallelSimulation(int nbParticules, int nbSteps, double mass, double dt, 
			     vnd_t *position, vnd_t *velocity, 
			     vnd_t *accel, vnd_t *force);

int main()
{
  int nbParticules = 4096;
  int nbSteps = 10;
  /* simulation parameters */
  double mass = 1.0;
  double dt = 1.0e-4;
  vnd_t *position;
  vnd_t *velocity;
  vnd_t *force;
  vnd_t *accel;

  position = (vnd_t *)malloc(nbParticules * sizeof(vnd_t));
  velocity = (vnd_t *)malloc(nbParticules * sizeof(vnd_t));
  force = (vnd_t *)malloc(nbParticules * sizeof(vnd_t));
  accel = (vnd_t *)malloc(nbParticules * sizeof(vnd_t));

  double serial_t0 = omp_get_wtime();
  serialSimulation(nbParticules, nbSteps, mass, dt, position, velocity, accel, force);
  double serial_t1 = omp_get_wtime();

  std::cout << " Done with serial version. " << std::endl;
    
  double parallel_t0 = omp_get_wtime();
  parallelSimulation(nbParticules, nbSteps, mass, dt, position, velocity, accel, force);
  double parallel_t1 = omp_get_wtime();

  std::cout << " Done with parallel version. " << std::endl;
    
  std::cout << "Serial version ran in " << (serial_t1 - serial_t0) << " seconds" << std::endl
  	    << "Parallel version ran in " <<  (parallel_t1 - parallel_t0) << " seconds" << std::endl
    	    << "Resulting in a speedup of " << (serial_t1 - serial_t0) / (parallel_t1 - parallel_t0) << std::endl;

  return 0;
}

/* -----------------------------------------------------------------------
   statement function for the pair potential.
   This potential is a harmonic well which smoothly saturates to a
   maximum value at PI/2.  
 * ----------------------------------------------------------------------- */
static double v(double x) 
{
  if (x < M_PI_2) 
    return pow(sin(x), 2.0);
  else
    return 1.0;
}

/* -----------------------------------------------------------------------
   statement function for the derivative of the pair potential
   * ----------------------------------------------------------------------- */
static double dv(double x) 
{
  if (x < M_PI_2) 
    return 2.0 * sin(x) * cos(x);
  else
    return 0.0;
}

/*
  Zeros the vector in parameter.
*/
static void clearVector(int size, vnd_t *data)
{
  for(int i = 0; i < size; ++i) {
    for(int j = 0; j < 3; ++j)
      data[i][j] = 0.0;
  }
}

/* -----------------------------------------------------------------------
   Initialize the positions, velocities, and accelerations.
   * ----------------------------------------------------------------------- */
static void initialize(int np, int nd, const vnd_t &box, vnd_t *pos, vnd_t *vel, vnd_t *acc) 
{
  srand(4711L);
  for (int i = 0; i < np; i++) {
    for (int j = 0; j < nd; j++) {
      double x = rand() % 10000 / (double)10000.0;
      pos[i][j] = box[j] * x;
      vel[i][j] = 0.0;
      acc[i][j] = 0.0;
    }
  }
}

/* -----------------------------------------------------------------------
   Compute the displacement vector (and its norm) between two particles. 
   * ----------------------------------------------------------------------- */
static double dist(int nd, const vnd_t &r1, const vnd_t &r2, vnd_t &dr) 
{
  int i;
  double d;
  
  d = 0.0;
  for (i = 0; i < nd; i++) {
    dr[i] = r1[i] - r2[i];
    d += dr[i] * dr[i];
  }
  return sqrt(d);
}

/* -----------------------------------------------------------------------
   Return the dot product between two vectors of type double and length n 
   * ----------------------------------------------------------------------- */
static double dot_prod(int n, const vnd_t &x, const vnd_t &y) 
{
  int i;
  double t = 0.0;
  
  for (i = 0; i < n; i++) {
    t += x[i] * y[i];
  }
  return t;
}

/* -----------------------------------------------------------------------
   Compute the forces and energies, given positions, masses, and velocities
   * ----------------------------------------------------------------------- */
void serialCompute(int np, int nd, vnd_t *pos, vnd_t *vel, double mass, 
		    vnd_t *f, double *pot_p, double *kin_p) 
{
  int j;
  vnd_t rij;
  double d, pot = 0.0, kin = 0.0;
  
  for (int i = 0; i < np; i++) {
    /* compute potential energy and forces */
    for (j = 0; j < nd; j++)
      f[i][j] = 0.0;
    for (j = 0; j < np; j++) {
      if (i != j) {
        d = dist(nd, pos[i], pos[j], rij);
        /* attribute half of the potential energy to particle 'j' */
        pot = pot + 0.5 * v(d);
        for (int k = 0; k < nd; k++) {
          f[i][k] = f[i][k] - rij[k]* dv(d) /d;
        }
      } 
    }
    /* compute kinetic energy */
    kin = kin + dot_prod(nd, vel[i], vel[j]);
  }
  kin = kin * 0.5 * mass;
  *pot_p = pot;
  *kin_p = kin;
}

void parallelCompute(int np, int nd, vnd_t *pos, vnd_t *vel, double mass, 
		    vnd_t *f, double *pot_p, double *kin_p) 
{
  double pot = 0.0, kin = 0.0;
  
  /* The computation of forces and energies is fully parallel. */
#pragma omp parallel for default(shared) firstprivate(np, nd) reduction(+ : pot, kin)
  for (int i = 0; i < np; i++) {
    int j;
    /* compute potential energy and forces */
    for (j = 0; j < nd; j++)
      f[i][j] = 0.0;
    for (j = 0; j < np; j++) {
      if (i != j) {
	vnd_t rij;
        double d = dist(nd, pos[i], pos[j], rij);
        /* attribute half of the potential energy to particle 'j' */
        pot = pot + 0.5 * v(d);
        for (int k = 0; k < nd; k++) {
          f[i][k] = f[i][k] - rij[k]* dv(d) /d;
        }
      } 
    }
    /* compute kinetic energy */
    kin = kin + dot_prod(nd, vel[i], vel[j]);
  }
  kin = kin * 0.5 * mass;
  *pot_p = pot;
  *kin_p = kin;
}

/* -----------------------------------------------------------------------
   Perform the time integration, using a velocity Verlet algorithm
   * ----------------------------------------------------------------------- */
void serialUpdate(int np, int nd, vnd_t *pos, vnd_t *vel, vnd_t *f, vnd_t *a, 
		   double mass, double dt) 
{
  int i, j;
  double rmass;
  
  rmass = 1.0/mass;
  for (i = 0; i < np; i++) {
    for (j = 0; j < nd; j++) {
      pos[i][j] = pos[i][j] + vel[i][j]*dt + 0.5*dt*dt*a[i][j];
      vel[i][j] = vel[i][j] + 0.5*dt*(f[i][j]*rmass + a[i][j]);
      a[i][j] = f[i][j]*rmass;
    }
  }
}

void parallelUpdate(int np, int nd, vnd_t *pos, vnd_t *vel, vnd_t *f, vnd_t *a, 
		   double mass, double dt) 
{
  double rmass = 1.0/mass;

  /* The time integration is fully parallel */
#pragma omp parallel for default(shared) firstprivate(rmass, dt, np, nd)
  for (int i = 0; i < np; i++) {
    for (int j = 0; j < nd; j++) {
      pos[i][j] = pos[i][j] + vel[i][j]*dt + 0.5*dt*dt*a[i][j];
      vel[i][j] = vel[i][j] + 0.5*dt*(f[i][j]*rmass + a[i][j]);
      a[i][j] = f[i][j]*rmass;
    }
  }
}

void serialSimulation(int nbParticules, int nbSteps, double mass, double dt, 
			     vnd_t *position, vnd_t *velocity, 
			     vnd_t *accel, vnd_t *force)
{
  double potential, kinetic, E0;
  int i;
  vnd_t box;

  for (i = 0; i < 3; i++)
    box[i] = 10.0;

  clearVector(nbParticules, force);
  /* set initial positions, velocities, and accelerations */
  initialize(nbParticules, 3, box, position, velocity, accel);
  /* compute the forces and energies */
  serialCompute(nbParticules, 3, position, velocity, mass, force, &potential, &kinetic);
  E0 = potential + kinetic;
  /* This is the main time stepping loop */
  for (i = 0; i < nbSteps; i++) {
    serialCompute(nbParticules, 3, position, velocity, mass, force, &potential, &kinetic);
    printf("%17.9e %17.9e %17.9e\n", potential, kinetic, (potential + kinetic - E0) / E0);
    serialUpdate(nbParticules, 3, position, velocity, force, accel, mass, dt);
  }
}

void parallelSimulation(int nbParticules, int nbSteps, double mass, double dt, 
			     vnd_t *position, vnd_t *velocity, 
			     vnd_t *accel, vnd_t *force)
{
  double potential, kinetic, E0;
  int i;
  vnd_t box;

  for (i = 0; i < 3; i++)
    box[i] = 10.0;

  clearVector(nbParticules, force);
  /* set initial positions, velocities, and accelerations */
  initialize(nbParticules, 3, box, position, velocity, accel);
  /* compute the forces and energies */
  parallelCompute(nbParticules, 3, position, velocity, mass, force, &potential, &kinetic);
  E0 = potential + kinetic;
  /* This is the main time stepping loop */
  for (i = 0; i < nbSteps; i++) {
    parallelCompute(nbParticules, 3, position, velocity, mass, force, &potential, &kinetic);
    printf("%17.9e %17.9e %17.9e\n", potential, kinetic, (potential + kinetic - E0) / E0);
    parallelUpdate(nbParticules, 3, position, velocity, force, accel, mass, dt);
  }
}
