/*
  The area under the curve y=4/(1+x*x) between 0 and 1 provides a way to compute Pi.
  The value of this integral can be approximated using a sum.  
*/

#include <omp.h>
#include <iostream>
#include <string>

static double serialPi(long precision);
static double parallelPi(long precision);

int main()
{
  double serialRes, parallelRes;
  double PI25DT = 3.141592653589793238462643;
  long precision = 1000000000;

  double serial_t0 = omp_get_wtime();
  serialRes = serialPi(precision);
  double serial_t1 = omp_get_wtime();

  std::cout << " Done with serial version: " << serialRes << std::endl;

  double parallel_t0 = omp_get_wtime();
  parallelRes = parallelPi(precision);
  double parallel_t1 = omp_get_wtime();

  std::cout << " Done with parallel version: " << parallelRes << std::endl;

  std::cout << " Error serial: " << (PI25DT- serialRes) << std::endl
	    << " Error parallel: " << (PI25DT- parallelRes) << std::endl
	    << " Diff: " << (serialRes-parallelRes) << std::endl;
    
  std::cout << "Serial version ran in " << (serial_t1 - serial_t0) << " seconds" << std::endl
	    << "Parallel version ran in " <<  (parallel_t1 - parallel_t0) << " seconds" << std::endl
	    << "Resulting in a speedup of " << (serial_t1 - serial_t0) / (parallel_t1 - parallel_t0) << std::endl;

  return 0;
}

static double serialPi(long precision)
{
  
  double w = 1.0 / precision;
  double pi = 0.0;
  double local;

  for(int i = 0; i < precision; i++) {
    local = (i + 0.5) * w;
    pi += 4.0 / (1.0 + local * local);
  }
  pi *= w;

  return pi;
}

static double parallelPi(long precision)
{
  
  double w = 1.0 / precision;
  double pi = 0.0;
  double local;
  int i;

#pragma omp parallel for default(shared) private(i,local) reduction(+:pi)
  for(i = 0; i < precision; i++) {
    local = (i + 0.5) * w;
    pi += 4.0 / (1.0 + local * local);
  }

  pi *= w;

  return pi;
}

