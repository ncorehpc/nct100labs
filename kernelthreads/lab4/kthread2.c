/*-----------------------------------------------------------------------------
 * Module that creates to instances of a kernel thread: ping and pong. 
 * The kernel threads should alternately display their names.
 * A thread parameter package is associated with each thread instance.
 * It contains the thread's name a wait queue to synchronize the two
 * thread instances, a condition variable used with the wait queue,
 * a pointer to the package of the other thread.
 * The kernel thread performs a loop
 *   conditionally block on a wait queue using wait_event_interruptible
 *   output name
 *   wait a while
 *   unblock the other thread using wake_up_interruptible.
 *
 * To let thread ping begin, set the initial value of its condition
 * variable to true (nonzero). 
 * To make thread pong wait set the initial value of its condition
 * variable to false (zero).
 * Use the kernel function mdelay() to busy wait the threads. Combine
 * this with a call to msleep() that makes the thread sleep for some
 * milliseconds so that the kernel does not hang.
 *
 * file: kthread2.c
 *
 * Franz Meyer, Mar/2010
 *-----------------------------------------------------------------------------*/

#include <linux/module.h>	/* Specifically, a module */
#include <linux/kernel.h>	/* We're doing kernel work */
#include <linux/init.h>         /* modules_init/_exit macros */
#include <linux/kthread.h>	/* kthread_run*/
#include <linux/sched.h>	/* task_struct*/
#include <linux/delay.h>	/* mdelay()*/
	
MODULE_LICENSE("Dual BSD/GPL");
/*
 * debug macro
 */
//#define DEBUG
#undef DEBUG

#ifdef DEBUG
#define DPRINT(fmt,args...) printk(KERN_INFO "%s,%i:" fmt "\n", \
                            __FUNCTION__, __LINE__,##args);
#else
#define DPRINT(fmt,args...)
#endif
/*
 * data package passed to threads
 */
struct kthr_data {
	const char *name;	/* kthread's name */
	wait_queue_head_t wqh; 	/* kthread waits on this wq */
	int cond;		/* condidition associated with wq*/
	struct kthr_data *other;
};
static struct kthr_data dping;
static struct kthr_data dpong;
/*
 * the kernel thread
 */
int kthread_fct(void *data)
{
	struct kthr_data *pdata = (struct kthr_data*)data;
	while(1) {
		DPRINT("wait(%s)", pdata->name);
		wait_event_interruptible(pdata->wqh, pdata->cond);
		pdata->cond = 0;
		printk(KERN_INFO "%s\n", pdata->name);
		mdelay(500);
		msleep(1);
		pdata->other->cond = 1;
		DPRINT("wakeup(%s)\n", pdata->name);
		wake_up_interruptible(&pdata->other->wqh);
		if (kthread_should_stop())
			break;
	}
	return 0;
}

struct task_struct *tping, *tpong;


int __init kthr_init(void)
{
	printk(KERN_INFO "init_module() called\n");
	init_waitqueue_head(&dping.wqh);
	init_waitqueue_head(&dpong.wqh);
	dping.cond = 1;
	dpong.cond = 0;	
	dping.name = "ping";
	dpong.name = "pong";
	dping.other = &dpong;
	dpong.other = &dping;

	tping = kthread_run(kthread_fct, &dping, "kping");
	tpong = kthread_run(kthread_fct, &dpong, "kpong");
	return 0;
}


void __exit kthr_exit(void)
{
	printk(KERN_INFO "cleanup_module() called\n");
	kthread_stop(tping);
	kthread_stop(tpong);
}

module_init(kthr_init);
module_exit(kthr_exit);

