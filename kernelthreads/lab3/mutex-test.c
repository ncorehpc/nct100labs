/** 
 * mutex-test.c: run some threads to test a mutex
 *                 as synchronisation primitives
 *  Notes
 *   - module parameters and descriptions, see code
 *   - does not really load the module, the tests (do_tests())
 *     are performed in the module init function. Then the
 *     init function returns with an error code. The module
 *     utility unloads the module and reports the error.
 *   - reschedule in CS and NCS
 *     On a UP machine the test is harder when you force
 *     rescheduling in the critical section (do_sched_in_cs=1)  
 *
 *   "load" the module (only execute do_tests())
 *    $ sudo /sbin/insmod ./mutex-test.ko mx=5 do_sched=1
 *
 */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/init.h>
#include <linux/kthread.h>
#include <linux/mutex.h>
#include <linux/random.h>
#include <linux/delay.h>

#define MAX_THREADS 64
/**
 * module parameters
 */
static int num_mtx_thr = 1; /*deft number of mutex test threads*/
static int elapse = 5;      /*elapse (run) time in seconds*/ 
static int load_in_cs = 5;  /*default value of load in cs in us*/
static int do_sched=0;      /*flag: schedule in NCS*/
static int do_sched_in_cs=0;/*flag: schedule in CS*/
static int load_in_ncs = 5; /*default value of load in NCS in us*/
static int verbose = 0;     /*verbosity of output*/

MODULE_AUTHOR("Franz Meyer");
MODULE_DESCRIPTION("Synchronisation primitive test demo");
MODULE_LICENSE("GPL");

module_param_named(v, verbose, int, 0);
MODULE_PARM_DESC(verbose, "Verbosity");

module_param_named(mx, num_mtx_thr, int, 0);
MODULE_PARM_DESC(num_mtx_thr, "Number of mutex threads");

module_param(elapse, int, 0);
MODULE_PARM_DESC(elapse, "Number of seconds to run for");

module_param(load_in_cs, int, 0);
MODULE_PARM_DESC(load_in_cs, "Length of load in uS in CS");

module_param(load_in_ncs, int, 0);
MODULE_PARM_DESC(load_in_ncs, "Length of interval in uS before re-getting lock (NCS)");

module_param(do_sched, int, 0);
MODULE_PARM_DESC(do_sched, "True if each thread schedules in NCS");
module_param(do_sched_in_cs, int, 0);
MODULE_PARM_DESC(do_sched_in_cs, "True if each thread schedules in CS");

/* the synchronization objects under test */
static struct mutex ____cacheline_aligned mutex;
/* timer to control elapse time */
static struct timer_list ____cacheline_aligned timer;
/* flag: run threads while working is true */
static atomic_t ____cacheline_aligned working = ATOMIC_INIT(0);

static int ____cacheline_aligned currentlyInsideCS;

static struct task_struct ____cacheline_aligned *mtx_tsk[MAX_THREADS];


/* counters for statistics */
static unsigned int ____cacheline_aligned mutex_taken[MAX_THREADS];
static unsigned int ____cacheline_aligned mutex_qlen[MAX_THREADS];

/**
 * get a random variable between zero and val, mean is val/2
 */
static int random_val(int val)
{
	unsigned short rand_val;
	get_random_bytes(&rand_val, sizeof(rand_val));
	return (rand_val % val);
}
/**
 * determine the number of waiters in the mutex queue
 *    use a spinlock while scanning the queue
 */
static int n_mutex_waiters(void)
{
	unsigned long flags;
	unsigned int waiters = 0;
	struct list_head *pln;
	spin_lock_irqsave(&mutex.wait_lock, flags);
	__list_for_each(pln, &mutex.wait_list){
		waiters++;
	}
	spin_unlock_irqrestore(&mutex.wait_lock, flags);
	return waiters;
}

static int mutex_thread(void *arg)
{
	unsigned int N = (unsigned long) arg;

	daemonize("Mutex%u", N);
	if (verbose >= 2)
		printk("%s: started\n", current->comm);
	set_user_nice(current, 19);

	while (!kthread_should_stop()) {
		mutex_lock(&mutex);
		mutex_taken[N]++;
		currentlyInsideCS = N;	
		mutex_qlen[n_mutex_waiters()]++;	
		if (load_in_cs)
			udelay(random_val(load_in_cs));
		if (do_sched_in_cs)
			schedule();
		if (load_in_cs)
			udelay(random_val(load_in_cs));
		if (currentlyInsideCS != N)
			printk("**** mutex-thread: other inside ****\n");
		mutex_unlock(&mutex);
		if (load_in_ncs)
			udelay(random_val(load_in_ncs));
		if (do_sched)
			schedule();
		if (load_in_ncs)
			udelay(random_val(load_in_ncs));
	}
	if (verbose >= 2)
		printk("%s: done\n", current->comm);
	return 0;
}
/** 
 * interval timer function: stop threads 
 */
static void stop_test(unsigned long dummy)
{
	atomic_set(&working, 0);
}

/* 
 * calculate and output 
 */
static unsigned int total(const char *what, unsigned int counts[], int num)
{
	unsigned int tot = 0, cnt;
	int loop;

	for (loop = 0; loop < num; loop++) {
		cnt = counts[loop];
		if (cnt == 0) {
			continue;
		}
		tot += cnt;
	}
	if (verbose && tot > 0) {
		printk("%s:", what);
		for (loop = 0; loop < num; loop++) {
			cnt = counts[loop];
			if (cnt == 0)
				printk(" zzz");
			else
				printk(" %d[%d%%]", cnt, cnt * 100 / tot);
		}
		printk("\n");
	}
	return tot;
}

static void show_parameters(void)
{
	printk("\nModule mutex-test: parameters\n"
	    "# of online CPUs      :%d\n"
	    "# of mutex threads    :%d\n"
	    "scheduling in NCS     :%s\n"
	    "scheduling in CS      :%s\n"
	    "load in CS in usec    :%d\n"
	    "load in NCS in usec   :%d\n"
	    "elapsed time in sec   :%d\n",
	num_online_cpus(), num_mtx_thr,
	do_sched ? "yes" : "no", do_sched_in_cs ? "yes" : "no",
	load_in_cs, load_in_ncs, elapse);	
}

/**
 * module init function
 */
static int __init do_tests(void)
{
	unsigned long loop;
	unsigned int mutex_total;

	if (num_mtx_thr < 0 || num_mtx_thr > MAX_THREADS ||
	    elapse < 1 || load_in_cs < 0 || load_in_cs > 999 ||
	    load_in_ncs < 0 || load_in_ncs > 999
	    ) {
		printk("Parameter out of range\n");
		return -ERANGE;
	}

	if ((num_mtx_thr) == 0) {
		int num = num_online_cpus();

		if (num > MAX_THREADS)
			num = MAX_THREADS;
		num_mtx_thr = num;
		load_in_cs = 1;
		load_in_ncs = 1;
		do_sched = 1;
		printk("No parameters - using defaults.\n");
	}
	show_parameters();

	/* random value test */
	
	printk("random value test:");
	for (loop = 0; loop < 10; loop++) {
		printk(" %d",random_val(load_in_cs));
	}
	printk("\n");

	if (verbose)
		printk("\nStarting synchronisation primitive tests...\n");
	/* init synchronization objects, working = true */
	mutex_init(&mutex);
	/* set a stop timer */
	init_timer(&timer);
	timer.function = stop_test;
	timer.expires = jiffies + elapse * HZ;
	add_timer(&timer);
	atomic_set(&working, 1);
	/* kick off all the mutex threads */
	for (loop = 0; loop < num_mtx_thr; loop++) {
		mtx_tsk[loop] = kthread_run(mutex_thread, (void *) loop, "Mtx%lu", loop);
	}
	/*
	 * wait for interval timer
	 */
	while (atomic_read(&working)) {
		if (verbose)
			printk("init(): wait for stop...\n");
		msleep(500);
	}
	if (verbose)
		printk("\nStopping kthreads...\n");
	/* now wait until it's all done */
	for (loop = 0; loop < num_mtx_thr; loop++) {
		kthread_stop(mtx_tsk[loop]);
	}
	if (verbose)
		printk("\nKthreads stopped\n");

	if (mutex_is_locked(&mutex))
		printk(KERN_ERR "Mutex is still locked!\n");

	/* count up */
	printk("mutex taken per thread\n");
	mutex_total = total("MTX", mutex_taken, num_mtx_thr);
	printk("mutex queue length in mutex_lock()\n");
		      total("MTXQ", mutex_qlen, num_mtx_thr);

	/* print the results */
	if (verbose) {
		printk("total mutexes taken: %u\n", mutex_total);
	}
	/* tell insmod to discard the module */
	if (verbose)
		printk("Tests complete\n");
	return -ENOANO;

} /* end do_tests() */

module_init(do_tests);

