/*-----------------------------------------------------------------------------
 * Module that creates to instances of a kernel thread: ping and pong. 
 * Each instances waits for semaphore sem1 (its own semaphore), outputs its
 * name, and posts semaphore sem2 (the other's semaphore)
 *
 * file: kthread1.c
 *
 * Franz Meyer, Mar/2010
 *-----------------------------------------------------------------------------*/
#include <linux/module.h>	/* Specifically, a module */
#include <linux/kernel.h>	/* We're doing kernel work */
#include <linux/init.h>         /* modules_init/_exit macros */
#include <linux/kthread.h>	/* kthread_run*/
#include <linux/sched.h>	/* task_struct*/
#include <linux/semaphore.h>    /* semaphore,down(),up()*/
#include <linux/delay.h>	/* mdelay()*/
/*
 * debug macro
 */
#define DEBUG

#ifdef DEBUG
#define DPRINT(fmt,args...) printk(KERN_INFO "%s,%i:" fmt "\n", \
                            __FUNCTION__, __LINE__,##args);
#else
#define DPRINT(fmt,args...)
#endif

MODULE_LICENSE("Dual BSD/GPL");
/*
 * data package passed to threads
 */
struct kthr_data {
	const char *name;	/* kthread's name */
	struct semaphore *sem1; /* kthread waits for this semaphore */
	struct semaphore *sem2; /* kthread posts this sempahore */ 
};
static struct kthr_data dping, dpong;
static struct semaphore pingsem, pongsem;
static struct task_struct *tping, *tpong;
/*
 * semaphores used to sync ping with pong
 *   pingsem is created with initial count = 1 (unlocked)
 *   pongsem is created with initial count = 0 (locked)
 * the semaphores are passed to ping in the order pingsem,pongsem
 * the semaphores are passed to pong in the order pongsem,pingsem
 * thus, ping requests pingsem and posts pongsem, pong is blocked
 * on pongsem.
 * At the beginning: because pongsem is in "locked state" the thread
 * pong will be blocked if it runs first. Thus ping prints first, 
 * even if pong is started before ping.
 */
int kthread_fct(void *data)
{
	/******* INSERT YOUR CODE HERE *******/
    
	return 0;
}
/*
 * module init/exit functions
 */
int __init kthr_init(void)
{
	printk(KERN_INFO "init_module() called\n");

	/*
	 * create and initialze semaphores
	 * create and start kernel threads
	 */
	
	/******* INSERT YOUR CODE HERE *******/
	
	return 0;
}

void __exit kthr_exit(void)
{
	printk(KERN_INFO "cleanup_module() called\n");

	/*
	 * stop kernel threads
	 */
	
	/******* INSERT YOUR CODE HERE *******/
}

module_init(kthr_init);
module_exit(kthr_exit);
