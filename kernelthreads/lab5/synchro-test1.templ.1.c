/* synchro-test1.c: run some threads to test a mutex and a semaphore
 *                 as synchronisation primitives
 *  Notes
 *    
 *
 * Copyright (C) 2005, 2006 Red Hat, Inc. All Rights Reserved.
 * Written by David Howells (dhowells@redhat.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version
 * 2 of the License, or (at your option) any later version.
 *
 * Some modifications and simplifications by myf1
 *  -- comments added
 *  -- random load and interval time
 *  -- rescheduling in CS and NCS possible, interesting for the
 *     behavior on UP machines
 *
 * The module should be run as something like:
 *
 *	insmod synchro-test.ko mx=1
 *	insmod synchro-test.ko sm=2 ism=1
 *	insmod synchro-test.ko sm=2 ism=2
 */
#include <linux/module.h>
#include <linux/poll.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/init.h>
#include <asm/atomic.h>
#include <linux/personality.h>
#include <linux/smp_lock.h>
#include <linux/delay.h>
#include <linux/timer.h>
#include <linux/completion.h>
#include <linux/mutex.h>
#include <linux/random.h>

#define MAX_THREADS 64

/*
 * Turn on self-validation if we do a one-shot boot-time test:
 */
# define VALIDATE_OPERATORS
/**
 * module parameters
 */
static int num_mtx_thr = 1;	/*# of mutex test threads*/
static int num_sem_thr = 1;	/*# of sempahore test threads*/
static int sem_init = 1;	/*initial value of the semaphore*/
static int elapse = 5;		/*run time of test in secs*/
static int load = 2;		/*default load value in CS*/
static int do_sched=0;		/*flag: schedule in NCS*/
static int do_sched_in_CS=0;	/*flag: schedule in CS*/
static int interval = 2;	/*default interval in NCS*/
static int verbose = 0;		/*verbosity of output*/

MODULE_AUTHOR("David Howells/Franz Meyer");
MODULE_DESCRIPTION("Synchronisation primitive test demo");
MODULE_LICENSE("GPL");

module_param_named(v, verbose, int, 0);
MODULE_PARM_DESC(verbose, "Verbosity");

module_param_named(mx, num_mtx_thr, int, 0);
MODULE_PARM_DESC(num_mtx_thr, "Number of mutex threads");

module_param_named(sm, num_sem_thr, int, 0);
MODULE_PARM_DESC(num_sem_thr, "Number of semaphore threads");

module_param_named(ism, sem_init, int, 0);
MODULE_PARM_DESC(sem_init, "Initial semaphore value");

module_param(elapse, int, 0);
MODULE_PARM_DESC(elapse, "Number of seconds to run for");

module_param(load, int, 0);
MODULE_PARM_DESC(load, "Length of load in uS in CS");

module_param(interval, int, 0);
MODULE_PARM_DESC(interval, "Length of interval in uS before re-getting lock (NCS)");

module_param(do_sched, int, 0);
MODULE_PARM_DESC(do_sched, "True if each thread schedules in NCS");
module_param(do_sched_in_CS, int, 0);
MODULE_PARM_DESC(do_sched_in_CS, "True if each thread schedules in CS");

/* the synchronization objects under test */
static struct mutex ____cacheline_aligned mutex;
static struct semaphore ____cacheline_aligned sem;
/* variables to track who is inside CS*/
static int ____cacheline_aligned currentlyInsideMutexCS;
static int ____cacheline_aligned currentlyInsideSemCS;

/* flag: run threads while working is true */
static atomic_t ____cacheline_aligned working = ATOMIC_INIT(0);

#ifdef VALIDATE_OPERATORS
static atomic_t ____cacheline_aligned mutexes	 = ATOMIC_INIT(0);
static atomic_t ____cacheline_aligned semaphores = ATOMIC_INIT(0);
#endif
/* counters for statistics */
static unsigned int ____cacheline_aligned mutex_taken[MAX_THREADS];
static unsigned int ____cacheline_aligned semaphore_taken[MAX_THREADS];
static unsigned int ____cacheline_aligned mutex_qlen[MAX_THREADS];
static unsigned int ____cacheline_aligned semaphore_qlen[MAX_THREADS];

/* completions for thread join */
static struct completion ____cacheline_aligned mx_comp[MAX_THREADS];
static struct completion ____cacheline_aligned sm_comp[MAX_THREADS];

static struct timer_list ____cacheline_aligned timer;
/**
 * macros ACCOUNT: increment counter when an synch object has been 
 *                 aquired
 */
#define ACCOUNT(var, N) var##_taken[N]++;

#ifdef VALIDATE_OPERATORS
#define TRACK(var, dir) atomic_##dir(&(var))

#define CHECK(var, cond, val)						\
do {									\
	int x = atomic_read(&(var));					\
	if (unlikely(!(x cond (val))))					\
		printk("check [%s %s %d, == %d] failed in %s\n",	\
		       #var, #cond, (val), x, __func__);		\
} while (0)

#else
#define TRACK(var, dir)		do {} while(0)
#define CHECK(var, cond, val)	do {} while(0)
#endif
/**
 * return a random variable between [0..val-1], mean value is val/2
 */
static int random_val(int val)
{
	unsigned short rand_val;
	get_random_bytes(&rand_val, sizeof(rand_val));
	return (rand_val % val);
}
/**
 * enter/exit critical section based on mutex lock
 */
static inline void do_mutex_lock(unsigned int N)
{
	mutex_lock(&mutex);

	ACCOUNT(mutex, N);
	TRACK(mutexes, inc);
	CHECK(mutexes, ==, 1);
}

static inline void do_mutex_unlock(unsigned int N)
{
	CHECK(mutexes, ==, 1);
	TRACK(mutexes, dec);

	mutex_unlock(&mutex);
}
/**
 * determine the number of waiters in the mutex queue
 *    use a spinlock while scanning the queue
 */
static int n_mutex_waiters(void)
{

    /******* INSERT YOUR CODE HERE *******/
    
    return waiters;
}
/**
 * the mutex test kernel thread
 *  loop
 *	enter critical section: acquired mutex lock
 *	perhaps wait actively to simulate load before sched
 *	perhaps schedule
 *	perhaps wait actively to simulate load after sched
 *	exit critical section: release mutex lock
 *	perhaps wait actively to simulate load before sched
 *	perhaps schedule
 *	perhaps wait actively to simulate load after sched
 *  endloop
 *  signal completetion
 */
static int mutex_thread(void *arg)
{
	unsigned int N = (unsigned long) arg;

	daemonize("Mutex%u", N);
	if (verbose >= 2)
		printk("%s: started\n", current->comm);
	set_user_nice(current, 19);

	while (atomic_read(&working)) {

	/******* INSERT YOUR CODE HERE *******/

	}
	if (verbose >= 2)
		printk("%s: done\n", current->comm);
	complete_and_exit(&mx_comp[N], 0);
}
/**
 * enter/exit critical section based on sempahore
 *  IMPORTANT: initial value must be one
 */
static inline void do_down(unsigned int N)
{
	down(&sem);
	ACCOUNT(semaphore, N);
	TRACK(semaphores, inc);
	CHECK(semaphores, ==, 1);
}

static inline void do_up(unsigned int N)
{
	CHECK(semaphores, ==, 1);
	TRACK(semaphores, dec);
	up(&sem);
}
/**
 * determine the number of waiters in the semaphore queue
 *    use a spinlock while scanning the queue
 */
static int n_sem_waiters(void)
{
	/******* INSERT YOUR CODE HERE *******/

	return waiters;
}
/**
 * the semaphore test kernel thread
 *  loop
 *	enter critical section: acquired semaphore (down)
 *	perhaps wait actively to simulate load before sched
 *	perhaps schedule
 *	perhaps wait actively to simulate load after sched
 *	exit critical section: release semaphore (up)
 *	perhaps wait actively to simulate load before sched
 *	perhaps schedule
 *	perhaps wait actively to simulate load after sched
 *  endloop
 *  signal completetion
 */
static int semaphore_thread(void *arg)
{
	unsigned int N = (unsigned long) arg;

	daemonize("Sem%u", N);
	if (verbose >= 2)
		printk("%s: started\n", current->comm);
	set_user_nice(current, 19);

	while (atomic_read(&working)) {

	/******* INSERT YOUR CODE HERE *******/

	}
	if (verbose >= 2)
		printk("%s: done\n", current->comm);
	complete_and_exit(&sm_comp[N], 0);
}
/**
 * called when interval timer times out: stop threads 
 **/
static void stop_test(unsigned long dummy)
{
	atomic_set(&working, 0);
}
/** 
 * calculate and output 
 */
static unsigned int total(const char *what, unsigned int counts[], int num)
{
	unsigned int tot = 0, cnt;
	int loop;

	for (loop = 0; loop < num; loop++) {
		cnt = counts[loop];
		if (cnt == 0) {
			continue;
		}
		tot += cnt;
	}
	if (verbose && tot > 0) {
		printk("%s:", what);
		for (loop = 0; loop < num; loop++) {
			cnt = counts[loop];
			if (cnt == 0)
				printk(" zzz");
			else
				printk(" %d%%", cnt * 100 / tot);
		}
		printk("\n");
	}
	return tot;
}

static void show_parameters(void)
{
	printk("\nModule synchro-test1: parameters\n"
	    "# of mutex threads    :%d\n"
	    "# of semaphore threads:%d\n"
	    "semaphore init value  :%d\n"
	    "scheduling in NCS     :%s\n"
	    "scheduling in CS      :%s\n"
	    "load in CS in usec    :%d\n"
	    "load in NCS in usec   :%d\n"
	    "elapsed time in sec   :%d\n",
	num_mtx_thr, num_sem_thr, sem_init, 
	do_sched ? "yes" : "no", do_sched_in_CS ? "yes" : "no",
	load, interval, elapse);	
}

/**
 * module init function: setup parameters, create test threads
 * run threads, cancel and join threads, print statistics.
 */
static int __init do_tests(void)
{
	unsigned long loop;
	unsigned int mutex_total, sem_total;

	if (num_mtx_thr < 0 || num_mtx_thr > MAX_THREADS ||
	    num_sem_thr < 0 || num_sem_thr > MAX_THREADS ||
	    sem_init < 1 || elapse < 1 ||
	    load < 0 || load > 999 ||
	    interval < 0 || interval > 999
	    ) {
		printk("Parameter out of range\n");
		return -ERANGE;
	}

	if ((num_mtx_thr | num_sem_thr ) == 0) {
		int num = num_online_cpus();

		if (num > MAX_THREADS)
			num = MAX_THREADS;
		num_mtx_thr = num_sem_thr = num;
		load = 1;
		interval = 1;
		do_sched = 1;
		printk("No parameters - using defaults.\n");
	}
	show_parameters();

	/* random value test */
	
	printk("random value test:");
	for (loop = 0; loop < 10; loop++) {
		printk(" %d",random_val(load));
	}
	printk("\n");

	if (verbose)
		printk("\nStarting synchronisation primitive tests...\n");
	/* init synchronization objects, working = true */
	mutex_init(&mutex);
	sema_init(&sem, sem_init);
	atomic_set(&working, 1);

	/* kick off all the mutex and semaphore threads */
	for (loop = 0; loop < num_mtx_thr; loop++) {
		init_completion(&mx_comp[loop]);
		kernel_thread(mutex_thread, (void *) loop, 0);
	}
	for (loop = 0; loop < num_sem_thr; loop++) {
		init_completion(&sm_comp[loop]);
		kernel_thread(semaphore_thread, (void *) loop, 0);
	}
	/* set a stop timer */
	init_timer(&timer);
	timer.function = stop_test;
	timer.expires = jiffies + elapse * HZ;
	add_timer(&timer);

	/* now wait until it's all done */
	for (loop = 0; loop < num_mtx_thr; loop++)
		wait_for_completion(&mx_comp[loop]);

	for (loop = 0; loop < num_sem_thr; loop++)
		wait_for_completion(&sm_comp[loop]);

	atomic_set(&working, 0);
	del_timer(&timer);

	if (mutex_is_locked(&mutex))
		printk(KERN_ERR "Mutex is still locked!\n");

	/* count up */
	mutex_total = total("MTX", mutex_taken, num_mtx_thr);
		      total("MTXQ", mutex_qlen, num_mtx_thr);
	sem_total   = total("SEM", semaphore_taken, num_sem_thr);
		      total("SEMQ", semaphore_qlen, num_sem_thr);

	/* print the results */
	if (verbose) {
		printk("mutexes taken: %u\n", mutex_total);
		printk("semaphores taken: %u\n", sem_total);
	} else {
		char buf[30];

		sprintf(buf, "%d/%d", interval, load);

		printk("%3d %3d %c %5s %9u %9u\n",
		       num_mtx_thr, num_sem_thr, 
		       do_sched ? 's' : '-',
		       buf,
		       mutex_total,
		       sem_total);
	}

	/* tell insmod to discard the module */
	if (verbose)
		printk("Tests complete\n");
	return -ENOANO;

} /* end do_tests() */

module_init(do_tests);
