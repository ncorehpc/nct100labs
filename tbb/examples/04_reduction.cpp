/*
  The area under the curve y=4/(1+x*x) between 0 and 1 provides a way to compute Pi.
  The value of this integral can be approximated using a sum.
*/

#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_reduce.h"
#include "tbb/blocked_range.h"
#include "tbb/tick_count.h"
#include <iostream>
#include <string>

static double serialPi(long precision);
class ParallelPi {
  double pi_;
  long precision_;
public:
  ParallelPi(long precision) : pi_(0.0), precision_(precision) {}
  ParallelPi(ParallelPi & ppi, tbb::split) : pi_(0.0), precision_(ppi.precision_) {}
  void join(ParallelPi &rhs) {
    pi_ += rhs.pi_;
  }
  void operator()(const tbb::blocked_range<int> &range);
  double getPi() const {
    return pi_;
  }
};

int main()
{
  double serialRes, parallelRes;
  double PI25DT = 3.141592653589793238462643;
  long precision = 1000000000;
  
  tbb::task_scheduler_init init;
  
  tbb::tick_count serial_t0 = tbb::tick_count::now();
  serialRes = serialPi(precision);
  tbb::tick_count serial_t1 = tbb::tick_count::now();
  std::cout << " Done with serial version: " << serialRes << std::endl;
  
  tbb::tick_count parallel_t0 = tbb::tick_count::now();
  ParallelPi ppi(precision);
  tbb::parallel_reduce(tbb::blocked_range<int>(0, precision, 1), ppi, tbb::auto_partitioner()); 
  parallelRes = ppi.getPi();
  tbb::tick_count parallel_t1 = tbb::tick_count::now();
  std::cout << " Done with parallel version: " << parallelRes << std::endl;

  std::cout << " Error serial: " << (PI25DT- serialRes) << std::endl
	    << " Error parallel: " << (PI25DT- parallelRes) << std::endl
	    << " Diff: " << (serialRes-parallelRes) << std::endl;
  
  std::cout << "Serial version ran in " << (serial_t1 - serial_t0).seconds() << " seconds" << std::endl
	    << "Parallel version ran in " <<  (parallel_t1 - parallel_t0).seconds() << " seconds" << std::endl
	    << "Resulting in a speedup of " << (serial_t1 - serial_t0).seconds() / (parallel_t1 - parallel_t0).seconds() << std::endl;
  
  return 0;
}

static double serialPi(long precision)
{
  
  double w = 1.0 / precision;
  double pi = 0.0;
  double local;

  for(int i = 0; i < precision; i++) {
    local = (i + 0.5) * w;
    pi += 4.0 / (1.0 + local * local);
  }
  pi *= w;

  return pi;
}

void ParallelPi::operator()(const tbb::blocked_range<int> &range)
{
  double w = 1.0 / precision_;
  double local, temp = 0.0;

  for(int i = range.begin(); i != range.end(); ++i) {
    local = (i + 0.5) * w;
    temp += 4.0 / (1.0 + local * local);
  }
  temp *= w;
  pi_ += temp;
}

