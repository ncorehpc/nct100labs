#include "tbb/task_scheduler_init.h"
#include <iostream>

int main() 
{
  tbb::task_scheduler_init init;
  // code
  std::cout << "Initialized. Nb threads=" << tbb::task_scheduler_init::default_num_threads() << std::endl;

  return 0;
}
