#include "tbb/task_scheduler_init.h"
#include <iostream>

int main() 
{
  tbb::task_scheduler_init init(tbb::task_scheduler_init::deferred);
  // code
  init.initialize(tbb::task_scheduler_init::default_num_threads() * 2);
  std::cout << "Initialized. Nb threads=" << tbb::task_scheduler_init::default_num_threads() << std::endl;

  init.terminate();
  return 0;
}
