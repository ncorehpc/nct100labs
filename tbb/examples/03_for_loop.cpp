/*
  For each position in a string, the program displays the length of the largest 
  matching substring elsewhere in the string. 
  The program also displays the location of a largest match for each position.  
  Consider the string "babba" as an example. Starting at position 0, "ba" is 
  the largest substring with a match elsewhere in the string (position 3). 
*/

#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_for.h"
#include "tbb/blocked_range.h"
#include "tbb/tick_count.h"
#include <iostream>
#include <string>

static const size_t N = 19;

void serialSubStringFinder(const std::string &str, size_t *max_array, size_t *pos_array);

class ParallelSubStringFinder {
  const std::string str;
  size_t *max_array;                        
  size_t *pos_array;
  size_t _size;
public:        
  void operator() ( const tbb::blocked_range<size_t>& r ) const; 
  ParallelSubStringFinder(std::string &s, size_t *m, size_t *p) :
    str(s), max_array(m), pos_array(p), _size(str.size()) { }                   
};

int main()
{
  tbb::task_scheduler_init init;
  
  // initialization of the strings
  // str[0] = "a", str[1] = "b", str[2] = "ab", str[3] = "bab"
  // str[4] = "abbab", etc.
  std::string str[N] = { std::string("a"), std::string("b") };
  for (size_t i = 2; i < N; ++i) 
    str[i] = str[i-1] + str[i-2];
  
  std::string &stringToScan = str[N-1];
  size_t strSize = stringToScan.size();
  //std::cout << "String to scan:" << stringToScan << std::endl;
  size_t *parallelMaxStr = new size_t[strSize];
  size_t *serialMaxStr = new size_t[strSize];
  size_t *parallelPosStr = new size_t[strSize];
  size_t *serialPosStr = new size_t[strSize];
  std::cout << " Done building string." << std::endl;

  tbb::tick_count serial_t0 = tbb::tick_count::now();
  serialSubStringFinder(stringToScan, serialMaxStr, serialPosStr);
  tbb::tick_count serial_t1 = tbb::tick_count::now();
  std::cout << " Done with serial version." << std::endl;
  
  tbb::tick_count parallel_t0 = tbb::tick_count::now();
  tbb::parallel_for(tbb::blocked_range<size_t>(0, strSize, 100),
		    ParallelSubStringFinder(stringToScan, parallelMaxStr, parallelPosStr) );                   
  tbb::tick_count parallel_t1 = tbb::tick_count::now();
  std::cout << " Done with parallel version." << std::endl;

 for (size_t i = 0; i < strSize; ++i) {
   if (parallelMaxStr[i] != serialMaxStr[i] || parallelPosStr[i] != serialPosStr[i]) {
     std::cout << "ERROR: Serial and Parallel Results are Different!" << std::endl;
   }
 }
 std::cout << " Done validating results." << std::endl;

 std::cout << "Serial version ran in " << (serial_t1 - serial_t0).seconds() << " seconds" << std::endl
           << "Parallel version ran in " <<  (parallel_t1 - parallel_t0).seconds() << " seconds" << std::endl
           << "Resulting in a speedup of " << (serial_t1 - serial_t0).seconds() / (parallel_t1 - parallel_t0).seconds() << std::endl;
  
  return 0;
}

void serialSubStringFinder(const std::string &str, size_t *max_array, size_t *pos_array) 
{
  for ( size_t i = 0; i < str.size(); ++i ) {
    size_t max_size = 0, max_pos = 0; 
    for (size_t j = 0; j < str.size(); ++j) {
      if (j != i) {
	size_t limit = str.size() - ( i > j ? i : j );
	for (size_t k = 0; k < limit; ++k) {
	  if (str[i + k] != str[j + k]) 
	    break;
	  if (k > max_size) {
	    max_size = k;
	    max_pos = j;
	  }
	}
      }
    }
    max_array[i] = max_size;
    pos_array[i] = max_pos;
  }
}

void ParallelSubStringFinder::operator() ( const tbb::blocked_range<size_t>& r ) const 
{ 
  for ( size_t i = r.begin(); i != r.end(); ++i ) {             
    size_t max_size = 0, max_pos = 0;                            
    for (size_t j = 0; j < str.size(); ++j)               
      if (j != i) {                  
	size_t limit = str.size()-( i > j ? i : j );               
	for (size_t k = 0; k < limit; ++k) {
	  if (str[i + k] != str[j + k]) break;              
	  if (k > max_size) {
	    max_size = k;                       
	    max_pos = j;                                         
	  }                                                  
	}
      }                                                     
    max_array[i] = max_size;                    
    pos_array[i] = max_pos;      
  }        
}                   
