/*
  This example demonstrates the different possible scopes of a mutex.
*/

#include <pthread.h>
#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static void createThreads(void *(*func) (void *), int nthreads, void *args[]);
static void functionScopeTest();
static void objectScopeTest();

int main()
{
  functionScopeTest();
  objectScopeTest();

  return 0;
}

static void printString(const std::string &str)
{
  /* The mutex variable must be static to be the same instance for all threads. */
  static pthread_mutex_t IOmutex = PTHREAD_MUTEX_INITIALIZER;

  pthread_mutex_lock(&IOmutex);
  std::cerr << str << std::endl;
  pthread_mutex_unlock(&IOmutex);
}

struct FunctionScopeParam {
  int threadNb_;
  FunctionScopeParam(int threadNb) : threadNb_(threadNb) {}
};

static void *threadFunctionScope(void *paramPtr)
{
  static pthread_mutex_t functionMutex = PTHREAD_MUTEX_INITIALIZER;

  pthread_mutex_lock(&functionMutex);
  {
    std::ostringstream ostr;
    
    FunctionScopeParam *param = (FunctionScopeParam *)paramPtr;
    ostr << "Thread Nb " << param->threadNb_ << " started working.";
    printString(ostr.str());
    sleep(1);
    ostr.str("");
    ostr << "Thread Nb " << param->threadNb_ << " finished working.";
    printString(ostr.str());
  }
  pthread_mutex_unlock(&functionMutex);
  
  return NULL;
}

static void functionScopeTest()
{
  int nthreads = 3;
  FunctionScopeParam *params[nthreads];
  for(int i = 0; i < nthreads; ++i) {
    params[i] = new FunctionScopeParam(i + 1);
  }
  createThreads(threadFunctionScope, nthreads, (void **)params);
  for(int i = 0; i < nthreads; ++i) {
    delete params[i];
  }
  
}
 
class ObjectScope {
private:
  int objectNb_;
  pthread_mutex_t objectMutex_;
public:
  ObjectScope(int objectNb) : objectNb_(objectNb) {
    pthread_mutex_init(&objectMutex_, NULL);
  }
  ~ObjectScope() {
    pthread_mutex_destroy(&objectMutex_);
  }
  void objectMethod(int threadNb) {
    pthread_mutex_lock(&objectMutex_);
    std::ostringstream ostr;
    
    ostr << "Object Nb " << objectNb_ << " Thread Nb " << threadNb << 
      " started working.";
    printString(ostr.str());
    sleep(1);
    ostr.str("");
    ostr << "Object Nb " << objectNb_ << " Thread Nb " << threadNb << 
      " finished working.";
    printString(ostr.str());
    pthread_mutex_unlock(&objectMutex_);
  }
};

struct ObjectScopeParam {
  int threadNb_;
  ObjectScope *pObjectScope_;
  ObjectScopeParam(int threadNb, ObjectScope *pObjectScope) :
    threadNb_(threadNb), pObjectScope_(pObjectScope) {}
};

static void *objectFunctionScope(void *paramPtr)
{
  std::ostringstream ostr;
  
  ObjectScopeParam *param = (ObjectScopeParam *)paramPtr;
  param->pObjectScope_->objectMethod(param->threadNb_);

  return NULL;
}


static void objectScopeTest()
{
  int nthreads = 4, nobjects = nthreads / 2;
  ObjectScopeParam *params[nthreads];
  ObjectScope *objs[nobjects];
 
  for(int i = 0; i < nobjects; ++i) {
    objs[i] = new ObjectScope(i + 1);
  }
  for(int i = 0; i < nthreads; ++i) {
    params[i] = new ObjectScopeParam(i + 1, objs[i / 2]);
  }
  createThreads(objectFunctionScope, nthreads, (void **)params);
  for(int i = 0; i < nthreads; ++i) {
    delete params[i];
  }
  for(int i = 0; i < nobjects; ++i) {
    delete objs[i];
  }
  
}
 
static void createThreads(void *(*func) (void *), int nthreads, void *args[])
{
  int i;
  pthread_t threadIDs[nthreads];
  std::ostringstream ostr;

  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    /* Creation of one thread. */
    int status = pthread_create(&threadIDs[i], NULL, func, args[i]);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  ostr << nthreads << " threads have been created.";
  printString(ostr.str());

  /* From here it is necessary to wait for all the threads. */
  for(i = 0; i < nthreads; ++i) {
    int status = pthread_join(threadIDs[i], NULL);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  /* Here, all threads are finished, so there is no need for a mutex. */
  std::cerr << nthreads << " threads are finished." << std::endl;
}

