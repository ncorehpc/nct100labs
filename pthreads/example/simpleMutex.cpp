/*
 * This example demonstrates a simple use of a mutex.
 */

#include <pthread.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static pthread_mutex_t globalMutex = PTHREAD_MUTEX_INITIALIZER;

static void createThreads(void *(*func) (void *), int nthreads, void *args[]);
static void noLockTest();
static void lockTest();

int x = 0;

int main()
{
  noLockTest();
  lockTest();

  return 0;
}

struct ThreadParam {
  int threadNb_;
  ThreadParam(int threadNb) : threadNb_(threadNb) {}
};

static void *threadNoLockFunction(void *paramPtr)
{
  ThreadParam *param = (ThreadParam *)paramPtr;
  std::cerr << "Thread Nb " << param->threadNb_ << " started working." << std::endl;
  sleep(1);
  std::cerr << "Thread Nb " << param->threadNb_ << " finished working." << std::endl;

  return NULL;
}

static void noLockTest()
{
  int nthreads = 3;
  ThreadParam *params[nthreads];
  for(int i = 0; i < nthreads; ++i) {
    params[i] = new ThreadParam(i + 1);
  }
  createThreads(threadNoLockFunction, nthreads, (void **)params);
  for(int i = 0; i < nthreads; ++i) {
    delete params[i];
  }
}

static void *threadLockFunction(void *paramPtr)
{
  pthread_mutex_lock(&globalMutex);
  {
    ThreadParam *param = (ThreadParam *)paramPtr;
    std::cerr << "Thread Nb " << param->threadNb_ << " started working." << std::endl;
    sleep(1);
    std::cerr << "Thread Nb " << param->threadNb_ << " finished working." << std::endl;
  }
  pthread_mutex_unlock(&globalMutex);

  return NULL;
}

static void lockTest()
{
  int nthreads = 3;
  ThreadParam *params[nthreads];
  for(int i = 0; i < nthreads; ++i) {
    params[i] = new ThreadParam(i + 1);
  }
  createThreads(threadLockFunction, nthreads, (void **)params);
  for(int i = 0; i < nthreads; ++i) {
    delete params[i];
  }
}

static void createThreads(void *(*func) (void *), int nthreads, void *args[])
{
  int i;
  pthread_t threadIDs[nthreads];

  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    /* Creation of one thread. */
    int status = pthread_create(&threadIDs[i], NULL, func, args[i]);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }

  /* From here it is necessary to wait for all the threads. */
  for(i = 0; i < nthreads; ++i) {
    int status = pthread_join(threadIDs[i], NULL);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  /* Here, all threads are finished, so there is no need for a mutex. */
  std::cerr << nthreads << " threads are finished." << std::endl;
}

