/* 
 * This program demonstrates a simple use of a condition variable for delayed 
 * synchronization.
 */

#include <pthread.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* 
 * A condition variable works in pair with a mutex.
 * Few condition variable can be paired with a same mutex,
 * but a condition variable cannot be associated with more than one mutex.
 */
static pthread_mutex_t globalMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t globalCondVar = PTHREAD_COND_INITIALIZER;
static int globalData = 0;
/* This boolean is used to control when globalData contains an information. */
static bool dataAvailable = false;

static void createThreads(void *(*func) (void *), int nthreads, void *args[], 
			  int nbLoops);
static void condVarTest();

int main()
{
  condVarTest();

  return 0;
}

struct ThreadParam {
  int threadNb_;
  int nbLoops_;
  ThreadParam(int threadNb, int nbLoops) : threadNb_(threadNb), nbLoops_(nbLoops) {}
};

static void *threadLockFunction(void *paramPtr)
{
  pthread_mutex_lock(&globalMutex);
  {
    ThreadParam *param = (ThreadParam *)paramPtr;
    std::cerr << "Thread Nb " << param->threadNb_ << " started working." 
	      << std::endl;
    
    for (int i = 0; i < param->nbLoops_; ++i) {
      /* Test if globalData has been updated. */
      if(!dataAvailable) {
	/* 
	 * If not, let sleep until the main thread signal an update with 
	 * pthread_cond_signal.
	 */
	pthread_cond_wait(&globalCondVar, &globalMutex);
      }
      /* 
       * globalData is updated, AND globalMutex is locked.
       * The thread can "consume" globalData.
       */
      std::cerr << "Thread Nb " << param->threadNb_ << " Data = " << 
	globalData << std::endl;
      dataAvailable = false;
    }
    std::cerr << "Thread Nb " << param->threadNb_ << " finished working." 
	      << std::endl;
  }
  pthread_mutex_unlock(&globalMutex);

  return NULL;
}

static void testLoop(int nbLoops)
{
  /* 
   * This function is executed in the main thread.
   * It "produces" data for the "consumer" threads in an asynchronous way.
   */
  for(int i = 0; i < nbLoops; ++i) {
    std::cerr << "Main thread sleep." << std::endl;
    sleep(1);
    /* It is time to produce some data. */
    pthread_mutex_lock(&globalMutex);
    /* Here the thread can safely change globalData and dataAvailable. */
    globalData = i + 1;
    /*
     * To keep this program simple, it produces a data every second.
     * Since it is reasonable to assume that "consumer" threads can wake up 
     * and print an int in less than 1s, there is no need to check if 
     * previous data has already been "consumed".
     */
    dataAvailable = true;
    /* Let's wake up one (any) "consumer" thread. */
    pthread_cond_signal(&globalCondVar);
    pthread_mutex_unlock(&globalMutex);
  }
}

static void condVarTest()
{
  int nthreads = 2, nbLoops = 3;
  ThreadParam *params[nthreads];
  for(int i = 0; i < nthreads; ++i) {
    params[i] = new ThreadParam(i + 1, nbLoops);
  }

  createThreads(threadLockFunction, nthreads, (void **)params, nbLoops);

  for(int i = 0; i < nthreads; ++i) {
    delete params[i];
  } 
}
  
static void createThreads(void *(*func) (void *), int nthreads, void *args[],
			  int nbLoops)
{
  int i;
  pthread_t threadIDs[nthreads];

  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    /* Creation of one thread. */
    int status = pthread_create(&threadIDs[i], NULL, func, args[i]);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }

  testLoop(nbLoops * nthreads);

  /* From here it is necessary to wait for all the threads. */
  for(i = 0; i < nthreads; ++i) {
    int status = pthread_join(threadIDs[i], NULL);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  /* Here, all threads are finished, so there is no need for a mutex. */
  std::cerr << nthreads << " threads are finished." << std::endl;
}

