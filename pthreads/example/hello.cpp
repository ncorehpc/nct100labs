/******************************************************************************
 *   In this simple example, the primary thread create N threads, wait for them,
 *   then exit.
 *   All threads print their thread number and their thread ID.
 ******************************************************************************/
#include <pthread.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>

/* Function called by each thread after its creation. */
void *threadFunction(void *paramPtr)
{
  int param = *((int *)paramPtr);
  std::cerr << "Thread: " << param << " ID: " << (long)pthread_self() << std::endl;

  return NULL;
}

int main (int argc, char *argv[])
{
  int nthreads = 2, i;

  /* pthread_t are opaque data type used to identify threads. */
  pthread_t threadIDs[nthreads];

  /*
     Parameters for the threads must be stored separately since we only
     provide pointers on them.
  */
  int params[nthreads];

  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    params[i] = i + 1;
    /* Creation of one thread. */
    int status = pthread_create(&threadIDs[i], NULL, threadFunction, &params[i]);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  std::cerr << nthreads << " threads have been created." << std::endl;

  /* From here it is necessary to wait for all the threads. */
  for(i = 0; i < nthreads; ++i) {
    /*
       Wait for the thread which ID is given, store the return value in the
       second parameter if needed.
    */
    int status = pthread_join(threadIDs[i], NULL);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  std::cerr << nthreads << " threads are finished." << std::endl;

  return 0;
}

