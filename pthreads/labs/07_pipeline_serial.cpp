/*
 * This program simulate a "pipeline" structure.
 */

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static int stage1(int param);
static int stage2(int param);
static int stage3(int param);

int main() 
{
  int result, result1, result2, nsteps = 5;

  for(int i = 0; i < nsteps; ++i) {
    result1 = stage1(i);
    result2 = stage2(result1);
    result = stage3(result2);
    std::cerr << "Input " << i << " result " << result << std::endl;
  }
  return 0;
}

static int stage1(int param)
{
  std::cerr << "Stage 1 begin " << param << std::endl;
  sleep(1); 
  std::cerr << "Stage 1 end." << std::endl;
  return param * 10;
}

static int stage2(int param)
{
  std::cerr << "Stage 2 begin " << param << std::endl;
  sleep(2); // This stage is twice slower !
  std::cerr << "Stage 2 end." << std::endl;
  return param * 100;
}

static int stage3(int param)
{
  std::cerr << "Stage 3 begin " << param << std::endl;
  sleep(1); 
  std::cerr << "Stage 3 end." << std::endl;
  return param * 1000;
}
