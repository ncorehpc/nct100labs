/*
 * A thread pool remove the cost of thread creation when many tasks must be 
 * executed on a smaller number of processors.
 * N threads are first created then execute all tasks added to a task queue.
 * This pattern is particularly use in servers, each request being executed 
 * in a thread to improve latency, but the number of concurrent threads is 
 * bounded to avoid overload of the system.
 */

#include <pthread.h>
#include <iostream>
#include <string>
#include "async_queue.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static void executeRequest(int param)
{
  std::cerr << "Executing request " << param << std::endl;
  sleep(1);
}

struct Task {
  int taskNumber_;
  void (*call_function_)(int);
  int param_;

  Task(int taskNumber, void (*fct)(int), int param) :
    taskNumber_(taskNumber), call_function_(fct), param_(param) {}

  void run() {
    call_function_(param_);
  }
};

Queue<Task *> taskQueue(10);

struct ThreadParam {
  int threadNb_;
  ThreadParam(int threadNb) : threadNb_(threadNb) {}
};

static void *threadFunction(void *paramPtr)
{
  ThreadParam *param = (ThreadParam *)paramPtr;
  Task *taskPtr;

  // check for a bad param pointer
  if (NULL == param)
  {
      std::cerr<<"param is NULL"<<std::endl;
      exit(-1);
  }
  else
 {
  while(NULL != (taskPtr = taskQueue.dequeue())) {
    std::cerr << "Thread " << param->threadNb_ << " execute Task " <<
      taskPtr->taskNumber_ << std::endl;
    taskPtr->run();
    delete taskPtr;
  }
 }
  return NULL;
}

static void createThreads(pthread_t* ThreadIDs,void *(*func) (void *), int nthreads, void *args[]);
void joinThreads(pthread_t* ThreadIDs,int nthreads);

int main()
{
  int nthreads = 10, ntasks = 100;
  ThreadParam *params[nthreads];

  // allocate thread parameters
   for (int i = 0; i < nthreads; ++i)
   {
    params[i] = new ThreadParam(i + 1);
   }

  // create some threads
  pthread_t threadIDs[nthreads];
  createThreads(threadIDs,threadFunction, nthreads, (void **)params);

  // queue tasks
  for(int i = 0; i < ntasks; ++i) {
    taskQueue.enqueue(new Task(i + 1, executeRequest, i + 101));
  }

  // queue thread shutdown signal
  for(int i = 0; i < nthreads; ++i) {
    /* For each thread, we need to send a NULL to stop the loop. */
    taskQueue.enqueue(NULL);
  }

  // wait for threads to finish and delete params
  joinThreads(threadIDs,nthreads);

  for(int i = 0; i < nthreads; ++i) {
    delete params[i];
  }

  return 0;
}

static void createThreads(pthread_t* threadIDs, void *(*func) (void *), int nthreads, void *args[])
{
  int i;

  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    /* Creation of one thread. */
    int status = pthread_create(&threadIDs[i], NULL, func, args[i]);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
}

void joinThreads(pthread_t* threadIDs, int nthreads)
{
  /* From here it is necessary to wait for all the threads. */
  for(int i = 0; i < nthreads; ++i) {
    int status = pthread_join(threadIDs[i], NULL);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  /* Here, all threads are finished, so there is no need for a mutex. */
  std::cerr << nthreads << " threads are finished." << std::endl;
}
