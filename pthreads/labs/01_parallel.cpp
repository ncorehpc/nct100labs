/******************************************************************************
 *   In this simple example, three vectors A, B and are defined by:
 *   A[i] = B[i] = i
 *   C = A + B
 *   The primary thread create N threads, each one execute the computation on
 *   1/Nth of the range of the loop.
 ******************************************************************************/
#include <pthread.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <string.h>

typedef std::vector<double> Vector;

static void compute(const Vector &A, const Vector &B, Vector &C, int startIndex,
		    int endIndex);
static void init(Vector &A, Vector &B);
static bool check(const Vector &A, const Vector &B, Vector &C);

struct VectorsStruct {
  Vector &A_;
  Vector &B_;
  Vector &C_;
  VectorsStruct(Vector &A, Vector &B, Vector &C) : A_(A), B_(B), C_(C) {}
};

struct ThreadParam {
  int startIndex_;
  int endIndex_;
  int threadNb_;
  pthread_t threadID_;
  VectorsStruct &vectors_;
  ThreadParam(int startIndex, int endIndex, int threadNb, VectorsStruct &vectors)
    : startIndex_(startIndex), endIndex_(endIndex), threadNb_(threadNb),
      vectors_(vectors) {}
};

/* Function called by each thread after its creation. */
static void *threadFunction(void *paramPtr)
{
  ThreadParam *param = (ThreadParam *)paramPtr;

  std::cerr << "Thread: " << param->threadNb_ << " from " <<
    param->startIndex_ << " to " << param->endIndex_<< std::endl;
  compute(param->vectors_.A_, param->vectors_.B_, param->vectors_.C_,
	  param->startIndex_, param->endIndex_);
  return NULL;
}

int main (int argc, char *argv[])
{
  int nthreads = 3, i;
  const int size = 1024;
  Vector A(size), B(size), C(size);
  VectorsStruct vectors(A, B, C);
  std::vector<ThreadParam *> params;

  init(A, B);

  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    int startIndex = (size * i) / nthreads;
    int endIndex = (size * (i + 1)) / nthreads;
    ThreadParam *paramPtr = new ThreadParam(startIndex, endIndex, i + 1,
					    vectors);
    params.push_back(paramPtr);

    /* Creation of one thread. */
    int status = pthread_create(&paramPtr->threadID_, NULL, threadFunction,
				paramPtr);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  std::cerr << nthreads << " threads have been created." << std::endl;

  /* From here it is necessary to wait for all the threads. */
  for(i = 0; i < nthreads; ++i) {
    int status = pthread_join(params[i]->threadID_, NULL);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
    delete params[i];
  }
  std::cerr << nthreads << " threads are finished." << std::endl;
  std::cerr << "Computation is over ... Checking" << std::endl;
  if(check(A, B, C)) {
    std::cerr << "Check done." << std::endl;
  } else {
    std::cerr << "Something wrong." << std::endl;
  }

  return 0;
}

static void compute(const Vector &A, const Vector &B, Vector &C, int startIndex,
		    int endIndex)
{
  std::cerr << "Computing for " << A.size() << " elements." << std::endl;
  double sumC = 0.0, sumA = 0.0, sumB = 0.0;

  for(int i = startIndex; i < endIndex; ++i) {
    C[i] = A[i] + B[i];
    sumC += C[i];
    sumB += B[i];
    sumA += A[i];
  }
  std::cerr << "sums A:" << sumA << " B:" << sumB << " C:" << sumC << std::endl;
}

static void init(Vector &A, Vector &B)
{
  for(unsigned int i = 0; i < A.size(); ++i) {
    A[i] = B[i] = double(i);
  }
}

static bool check(const Vector &A, const Vector &B, Vector &C)
{
  for(unsigned int i = 0; i < C.size(); ++i) {
    if(C[i] != A[i] + B[i] || C[i] != 2.0 * i) {
      std::cerr << "Error " << C[i] << " <> " << A[i] << " + " << B[i]
		<< std::endl;
      return false;
    }
  }
  return true;
}
