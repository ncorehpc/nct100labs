/*
 * Reader-Writer locks optimize cases where reading a shared variable can be
 * safely done simultaneously by more than one thread. 
 * Writing must still be done with an exclusive access to the variable.
 */

#include <pthread.h>
#include <iostream>
#include <string>
#include <map>
#include <stdlib.h>
#include <string.h>

static void createThreads(void *(*func) (void *), int nthreads, void *args[]);

static int compute(const int param)
{
  std::cerr << "compute " << param << std::endl;
  return param * param;
}

class Cache
{
private:
  mutable pthread_rwlock_t rwlock_;
  std::map<int,int> map_;

public:
  Cache() {
    pthread_rwlock_init(&rwlock_, NULL);
  }

  ~Cache() {
    pthread_rwlock_destroy(&rwlock_);
  }

  bool get(const int val, int &result) const {
    bool cached;

    pthread_rwlock_rdlock(&rwlock_);
    std::map<int,int>::const_iterator iter = map_.find(val);
    if(iter != map_.end()) {
      cached = true;
      result = (*iter).second;
    } else {
      cached = false;
    }
    pthread_rwlock_unlock(&rwlock_);

    return cached;
  }

  void put(const int val, const int result) {
    pthread_rwlock_wrlock(&rwlock_);
    map_[val] = result;
    pthread_rwlock_unlock(&rwlock_);
  }
};

Cache cache;

struct ThreadParam {
  int threadNb_;
  ThreadParam(int threadNb) : threadNb_(threadNb) {}
};

static void *threadFunction(void *paramPtr)
{
  ThreadParam *param = (ThreadParam *)paramPtr;
  int nbElems = 10, nbLoops = 10;

  for(int i = 0; i < nbLoops; ++i) {
    for(int j = 0; j < nbElems; ++j) {
      int result;
      if(!cache.get(j, result)) {
	/*
	 * Since another thread can ask the same value at the same time,
	 * it is likely that the same value will be computed more than once in
	 * different threads.
	 * Once the cache updated, though, the readers will be able to access
	 * the cache simultaneously.
	 */
	std::cerr << "Thread " << param->threadNb_ << " update " << j << std::endl;
	cache.put(j, compute(j));
      }
    }
  }

  return NULL;
}

int main()
{
  int nthreads = 10, i;
  ThreadParam *params[nthreads];

  for(i = 0; i < nthreads; ++i) {
    params[i] = new ThreadParam(i + 1);
  }

  createThreads(threadFunction, nthreads, (void **)params);

  for(i = 0; i < nthreads; ++i) {
    delete params[i];
  }

  return 0;
}

static void createThreads(void *(*func) (void *), int nthreads, void *args[])
{
  int i;
  pthread_t threadIDs[nthreads];

  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    /* Creation of one thread. */
    int status = pthread_create(&threadIDs[i], NULL, func, args[i]);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }

  /* From here it is necessary to wait for all the threads. */
  for(i = 0; i < nthreads; ++i) {
    int status = pthread_join(threadIDs[i], NULL);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  /* Here, all threads are finished, so there is no need for a mutex. */
  std::cerr << nthreads << " threads are finished." << std::endl;
}


