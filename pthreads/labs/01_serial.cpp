/******************************************************************************
 *   In this simple example, three vectors A, B and are defined by:
 *   A[i] = B[i] = i
 *   C = A + B 
 ******************************************************************************/
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <string.h>

typedef std::vector<double> Vector;

static void compute(const Vector &A, const Vector &B, Vector &C)
{
  std::cerr << "Computing for " << A.size() << " elements." << std::endl;
  double sumC = 0.0, sumA = 0.0, sumB = 0.0;

  for(unsigned int i = 0; i < A.size(); ++i) {
    C[i] = A[i] + B[i];
    sumC += C[i];
    sumB += B[i];
    sumA += A[i];
  }
  std::cerr << "sums A:" << sumA << " B:" << sumB << " C:" << sumC << std::endl;
}

static void init(Vector &A, Vector &B)
{
  for(unsigned int i = 0; i < A.size(); ++i) {
    A[i] = B[i] = double(i);
  }
}

static bool check(const Vector &A, const Vector &B, Vector &C)
{
  for(unsigned int i = 0; i < C.size(); ++i) {
    if(C[i] != A[i] + B[i] || C[i] != 2.0 * i) {
      std::cerr << "Error " << C[i] << " <> " << A[i] << " + " << B[i]<< std::endl;
      return false;
    }
  }
  return true;
}

int main (int argc, char *argv[]) 
{
  const int size = 1024;
  Vector A(size), B(size), C(size);

  init(A, B);
  compute(A, B, C);
  std::cerr << "Computation is over ... Checking" << std::endl;
  if(check(A, B, C)) {
    std::cerr << "Check done." << std::endl;
  } else {
    std::cerr << "Something wrong." << std::endl;
  }

  return 0;
}

