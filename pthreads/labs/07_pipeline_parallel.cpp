/*
 * This program simulate a fully parallel "pipeline" structure.
 */

#include <pthread.h>
#include <iostream>
#include <string>
#include "async_queue.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static int stage1(int param);
static int stage2(int param);
static int stage3(int param);

static void createThreads(void *(*func) (void *), int nthreads, void *args[]);

struct Stage {
  /* Each stage needs 2 asynchronous queues. */
  Queue<int> &input_;
  Queue<int> &output_;
  /*
   * This parameter is only used to determined when the thread should stop.
   * A sentinel value is a much better solution.
   */
  int nbSteps_;
  /* Funtion to call on each input data. A functor is also possible. */
  int (*call_function_) (int);  

  Stage(Queue<int> &input, Queue<int> &output, int nbSteps, int (*fct)(int)) :
    input_(input), output_(output), nbSteps_(nbSteps), call_function_(fct) {}

  void run() {
    for(int i = 0; i < nbSteps_; ++i) {
      output_.enqueue(call_function_(input_.dequeue()));
    }
  }
};

static void *threadFunction(void *paramPtr)
{
  Stage *param = (Stage *)paramPtr;
  
  param->run();

  return NULL;
}

int main() 
{
  Queue<int> input(100), output(100), stage1_to_2(100), stage2_to_3(100);
  int nsteps = 4, i, nthreads = 4;
  Stage *stages[nthreads];

  stages[0] = new Stage(input, stage1_to_2, nsteps, stage1);
  /* Stage2 is twice longer, so we use 2 threads. */
  stages[1] = new Stage(stage1_to_2, stage2_to_3, nsteps / 2, stage2);
  stages[2] = new Stage(stage1_to_2, stage2_to_3, nsteps / 2, stage2);
  stages[3] = new Stage(stage2_to_3, output, nsteps, stage3);

  /* Let's load the pipeline with the data. */
  for(i = 0; i < nsteps; ++i) {
    input.enqueue(i);
    std::cerr << "Input " << i << std::endl;
  }

  /* Create all the threads and wait until they all finished. */
  createThreads(threadFunction, nthreads, (void **)stages);

  for(i = 0; i < nsteps; ++i) {
    /* Results are not necessary in the same order. */
    std::cerr << "Result " << output.dequeue() << std::endl;
  }

  for(i = 0; i < nthreads; ++i) {
    delete stages[i];
  }

  return 0;
}

static int stage1(int param)
{
  std::cerr << "Stage 1 begin " << param << std::endl;
  sleep(1);
  std::cerr << "Stage 1 end." << std::endl;
  return param * 10;
}

static int stage2(int param)
{
  std::cerr << "Stage 2 begin " << param << std::endl;
  sleep(2); // This stage is twice slower !
  std::cerr << "Stage 2 end." << std::endl;
  return param * 100;
}

static int stage3(int param)
{
  std::cerr << "Stage 3 begin " << param << std::endl;
  sleep(1);
  std::cerr << "Stage 3 end." << std::endl;
  return param * 1000;
}

static void createThreads(void *(*func) (void *), int nthreads, void *args[])
{
  int i;
  pthread_t threadIDs[nthreads];

  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    /* Creation of one thread. */
    int status = pthread_create(&threadIDs[i], NULL, func, args[i]);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }

  /* From here it is necessary to wait for all the threads. */
  for(i = 0; i < nthreads; ++i) {
    int status = pthread_join(threadIDs[i], NULL);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  /* Here, all threads are finished, so there is no need for a mutex. */
  std::cerr << nthreads << " threads are finished." << std::endl;
}

