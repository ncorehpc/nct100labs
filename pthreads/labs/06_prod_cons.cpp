/* 
 * This program demonstrates a simple case of Producer-Consumer architecture
 * using our asynchronous queue.
 */

#include <pthread.h>
#include <iostream>
#include <string>
#include "async_queue.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static Queue<int> queue(20);

static void createThreads(void *(*func) (void *), int nthreads, void *args[], 
			  int nbLoops);
static void producerConsumerTest();

int main()
{
  producerConsumerTest();

  return 0;
}

struct ThreadParam {
  int threadNb_;
  int nbLoops_;
  ThreadParam(int threadNb, int nbLoops) : threadNb_(threadNb), nbLoops_(nbLoops) {}
};

static void *threadFunction(void *paramPtr)
{
  ThreadParam *param = (ThreadParam *)paramPtr;
  std::cerr << "Thread # " << param->threadNb_ << " started working." << std::endl;

  for (int i = 0; i < param->nbLoops_; ++i) {
    int data = queue.dequeue();
    std::cerr << "Thread # " << param->threadNb_ << " Data = " << data  << std::endl;
  }
  std::cerr << "Thread # " << param->threadNb_ << " finished working." << std::endl;

  return NULL;
}

static void testLoop(int nbLoops)
{
  /* 
   * This function is executed in the main thread.
   * It "produces" data for the "consumer" threads in an asynchronous way.
   */
  for(int i = 0; i < nbLoops; ++i) {
    std::cerr << "Main thread sleep." << std::endl;
    sleep(1);
    /* It is time to produce some data. */
    queue.enqueue(i + 1);
  }
}

static void producerConsumerTest()
{
  int nthreads = 2, nbLoops = 3;
  ThreadParam *params[nthreads];
  for(int i = 0; i < nthreads; ++i) {
    params[i] = new ThreadParam(i + 1, nbLoops);
  }

  createThreads(threadFunction, nthreads, (void **)params, nbLoops);

  for(int i = 0; i < nthreads; ++i) {
    delete params[i];
  } 
}
  
static void createThreads(void *(*func) (void *), int nthreads, void *args[],
			  int nbLoops)
{
  int i;
  pthread_t threadIDs[nthreads];

  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    /* Creation of one thread. */
    int status = pthread_create(&threadIDs[i], NULL, func, args[i]);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }

  testLoop(nbLoops * nthreads);

  /* From here it is necessary to wait for all the threads. */
  for(i = 0; i < nthreads; ++i) {
    int status = pthread_join(threadIDs[i], NULL);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  /* Here, all threads are finished, so there is no need for a mutex. */
  std::cerr << nthreads << " threads are finished." << std::endl;
}



