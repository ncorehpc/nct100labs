/*
 * Simple queue of integers implemented using the STL queue.
 * The behaviour of this program is unpredictable:
 *  - it may crash if 2 threads mishandle the same pointer
 *  - it may return a wrong result
 *  - it may return a good result, the probability decreases when the number of 
 *    threads or the number of elements increase
 */

#include <queue>
#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

class IntQueue
{
private:
  std::queue<int> queue_;
public:
  void enqueue(int val) {
    queue_.push(val);
  }
  int dequeue() {
    int ret = queue_.front();
    queue_.pop();
    return ret;
  }
  int size() const {
    return queue_.size();
  }
};

struct ThreadParam {
  IntQueue &queue_;
  int nelems_;
  int threadNb_;
  pthread_t threadID_;
  ThreadParam(IntQueue &queue, int nelems, int threadNb) : 
    queue_(queue), nelems_(nelems), threadNb_(threadNb) {}
};

struct ReturnValue {
  long value_;
  ReturnValue(long value) : value_(value) {}
};

/* Function called by each thread after its creation. */
static void *threadFunction(void *paramPtr)
{
  ThreadParam *param = (ThreadParam *)paramPtr;
  long sum = 0;

  for(int i = 0; i < param->nelems_; ++i) {
    int val = param->queue_.dequeue();
    sum += val;
  }
  std::cerr << "Thread: " << param->threadNb_ << " sum " << sum << std::endl;

  return new ReturnValue(sum);
}

int main() 
{
  IntQueue queue;
  int nthreads = 10, nelems = 10000, i;
  long sum = 0;
  
  for(i = 0; i < nthreads * nelems; ++i) {
    queue.enqueue(i);
    sum += i; 
  }

  std::vector<ThreadParam *> params;
  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    ThreadParam *paramPtr = new ThreadParam(queue, nelems, i + 1);
    params.push_back(paramPtr);
					    
    /* Creation of one thread. */
    int status = pthread_create(&paramPtr->threadID_, NULL, threadFunction, 
				paramPtr);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  std::cerr << nthreads << " threads have been created." << std::endl;
  
  /* From here it is necessary to wait for all the threads. */
  long retSum = 0;
  for(i = 0; i < nthreads; ++i) {
    ReturnValue *retPtr;
    int status = pthread_join(params[i]->threadID_, (void **)&retPtr);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
    retSum += retPtr->value_;
    delete params[i];
    delete retPtr;
  }
  std::cerr << nthreads << " threads are finished sum " << sum << " result " <<
	    retSum << std::endl;
  
  return 0;
}
