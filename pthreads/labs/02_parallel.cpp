/*
  The area under the curve y=4/(1+x*x) between 0 and 1 provides a way to compute Pi.
  The value of this integral can be approximated using a sum.
*/
#include <pthread.h>
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <string.h>

static double serialPi(long precision);
static double parallelPi(long precision);

int main()
{
  double serialRes, parallelRes;
  double PI25DT = 3.141592653589793238462643;
  long precision = 100000;

  serialRes = serialPi(precision);
  std::cout << " Done with serial version: " << serialRes << std::endl;

  parallelRes = parallelPi(precision);
  std::cout << " Done with parallel version: " << parallelRes << std::endl;

  std::cout << " Error serial: " << (PI25DT- serialRes) << std::endl
	    << " Error parallel: " << (PI25DT- parallelRes) << std::endl
	    << " Diff: " << (serialRes-parallelRes) << std::endl;
    
  return 0;
}

static double serialPi(long precision)
{
  
  double w = 1.0 / precision;
  double pi = 0.0;
  double local;

  for(int i = 0; i < precision; i++) {
    local = (i + 0.5) * w;
    pi += 4.0 / (1.0 + local * local);
  }
  pi *= w;

  return pi;
}

struct ThreadParam {
  long startIndex_;
  long endIndex_;
  int threadNb_;
  pthread_t threadID_;
  long precision_;
  ThreadParam(long startIndex, long endIndex, int threadNb, long precision)
    : startIndex_(startIndex), endIndex_(endIndex), threadNb_(threadNb), 
      precision_(precision) {}
};

struct ReturnValue {
  double pi_;
  ReturnValue(double pi) : pi_(pi) {}
};

static double partialPi(long precision, long startIndex, long endIndex)
{
  double w = 1.0 / precision;
  double pi = 0.0;
  double local;

  for(int i = startIndex; i < endIndex; i++) {
    local = (i + 0.5) * w;
    pi += 4.0 / (1.0 + local * local);
  }
  pi *= w;

  return pi;
}

static void *threadFunction(void *paramPtr)
{
  ThreadParam *param = (ThreadParam *)paramPtr;

  std::cerr << "Thread: " << param->threadNb_ << " from " <<  
    param->startIndex_ << " to " << param->endIndex_<< std::endl;
  double localPi = partialPi(param->precision_, param->startIndex_, 
			     param->endIndex_);

  return new ReturnValue(localPi);
}

static double parallelPi(long precision)
{
  int nthreads = 3, i;
  std::vector<ThreadParam *> params;
  
  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    long startIndex = (precision * i) / nthreads;
    long endIndex = (precision * (i + 1)) / nthreads;
    ThreadParam *paramPtr = new ThreadParam(startIndex, endIndex, i + 1, 
					    precision);
    params.push_back(paramPtr);
					    
    /* Creation of one thread. */
    int status = pthread_create(&paramPtr->threadID_, NULL, threadFunction, 
				paramPtr);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }
  std::cerr << nthreads << " threads have been created." << std::endl;

  /* From here it is necessary to wait for all the threads. */
  double pi = 0.0;
  for(i = 0; i < nthreads; ++i) {
    ReturnValue *retPtr;
    int status = pthread_join(params[i]->threadID_, (void **)&retPtr);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
    pi += retPtr->pi_;
    delete params[i];
    delete retPtr;
  }
  std::cerr << nthreads << " threads are finished." << std::endl;
  return pi;
}

