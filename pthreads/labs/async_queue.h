#ifndef _ASYNC_QUEUE_H_
#define _ASYNC_QUEUE_H_

/*
 * Safe queue implemented using the STL queue.
 */

#include <queue>
#include <pthread.h>

class ScopeLock {
private:
  pthread_mutex_t &lock_;

public:
  ScopeLock(pthread_mutex_t &lock) : lock_(lock) {
    pthread_mutex_lock(&lock_);
  }
  ~ScopeLock() {
    pthread_mutex_unlock(&lock_);
  }
};

template <typename T> class Queue
{
private:
  pthread_mutex_t mutex_;
  pthread_cond_t more_;
  pthread_cond_t less_;
  std::queue<T> queue_;
  size_t bound_;

public:
  Queue(size_t bound) : bound_(bound) {
    pthread_mutex_init(&mutex_, NULL);
    pthread_cond_init(&less_, NULL);
    pthread_cond_init(&more_, NULL);
  }
  ~Queue() {
    pthread_mutex_destroy(&mutex_);
    pthread_cond_destroy(&more_);
    pthread_cond_destroy(&less_);
  }
  void enqueue(T val) {
    ScopeLock lock(mutex_);
    while(queue_.size() >= bound_) // no spurious wakeups
        pthread_cond_wait(&less_,&mutex_);
    queue_.push(val);
    pthread_cond_signal(&more_);
  }
  T dequeue() {
    ScopeLock lock(mutex_);
    while(queue_.size() == 0) {
      pthread_cond_wait(&more_, &mutex_);
    }
    T ret = queue_.front();
    queue_.pop();
    pthread_cond_signal(&less_);
    return ret;
  }
  int size() {
    ScopeLock lock(mutex_);
    return queue_.size();
  }
};

#endif /* _ASYNC_QUEUE_H_ */
