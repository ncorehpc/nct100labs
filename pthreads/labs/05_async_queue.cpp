/*
 * Safe queue of integers implemented using the STL queue.
 */

#include <queue>
#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

class ScopeLock {
private:
  pthread_mutex_t &lock_;

public:
  ScopeLock(pthread_mutex_t &lock) : lock_(lock) {
    pthread_mutex_lock(&lock_);
  }
  ~ScopeLock() {
    pthread_mutex_unlock(&lock_);
  }
};

class IntQueue
{
private:
  pthread_mutex_t mutex_;
  pthread_cond_t more_;
  pthread_cond_t less_;
  std::queue<int> queue_;
  size_t bound_;

public:
  IntQueue(size_t bound) : bound_(bound) {
    pthread_mutex_init(&mutex_, NULL);
    pthread_cond_init(&less_, NULL);
    pthread_cond_init(&more_, NULL);
  }
  ~IntQueue() {
    pthread_mutex_destroy(&mutex_);
    pthread_cond_destroy(&more_);
    pthread_cond_destroy(&less_);
  }
  void enqueue(int val) {
    ScopeLock lock(mutex_);
    while(queue_.size() >= bound_) // no spurious wakeups
        pthread_cond_wait(&less_,&mutex_);
    queue_.push(val);
    pthread_cond_signal(&more_);
  }
  int dequeue() {
    ScopeLock lock(mutex_);
    while(queue_.size() == 0) {
      pthread_cond_wait(&more_, &mutex_);
    }
    int ret = queue_.front();
    queue_.pop();
    pthread_cond_signal(&less_);
    return ret;
  }
  int size() {
    ScopeLock lock(mutex_);
    return queue_.size();
  }
};

struct ThreadParam {
  IntQueue &queue_;
  int nelems_;
  int threadNb_;
  pthread_t threadID_;
  ThreadParam(IntQueue &queue, int nelems, int threadNb) : 
    queue_(queue), nelems_(nelems), threadNb_(threadNb) {}
};

struct ReturnValue {
  long value_;
  ReturnValue(long value) : value_(value) {}
};

/* Function called by each thread after its creation. */
static void *threadFunction(void *paramPtr)
{
  ThreadParam *param = (ThreadParam *)paramPtr;
  long sum = 0;

  for(int i = 0; i < param->nelems_; ++i) {
    int val = param->queue_.dequeue();
    sum += val;
  }

  return new ReturnValue(sum);
}

int main() 
{
  IntQueue queue(20);
  int nthreads = 10, nelems = 10000, i;
  long sum = 0;

  std::vector<ThreadParam *> params;
  /* Creation of the threads. */
  for(i = 0; i < nthreads; ++i) {
    ThreadParam *paramPtr = new ThreadParam(queue, nelems, i + 1);
    params.push_back(paramPtr);

    /* Creation of one thread. */
    int status = pthread_create(&paramPtr->threadID_, NULL, threadFunction, paramPtr);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_create failed:" << strerror(status) << std::endl;
      exit(-1);
    }
  }

  std::cerr << nthreads << " threads have been created." << std::endl;

  for(i = 0; i < nthreads * nelems; ++i) {
    queue.enqueue(i);
    sum += i;
  }

  sleep(1);
  /* From here it is necessary to wait for all the threads. */
  long retSum = 0;
  for(i = 0; i < nthreads; ++i) {
    ReturnValue *retPtr;
    int status = pthread_join(params[i]->threadID_, (void **)&retPtr);
    /* Error checking. */
    if(status != 0) {
      std::cerr << "pthread_join failed:" << strerror(status) << std::endl;
      exit(-1);
    }
    retSum += retPtr->value_;
    delete params[i];
    delete retPtr;
  }
  std::cerr << nthreads << " threads are finished sum " << sum << " result " <<
	    retSum << std::endl;

  return 0;
}
